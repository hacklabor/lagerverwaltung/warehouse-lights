#include <Arduino.h>
#include <main.h>

void setup()
{
  Serial.begin(115200);
  matrix.begin();
  matrix.fillScreen(matrix.Color(0, 100, 0));
  matrix.show();

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
   matrix.fillScreen(matrix.Color(0, 0, 0));
  matrix.show();
 
}

void loop()

{ matrix.fillScreen(matrix.Color(0, 0, 0));
ledChange=false;
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();
  if (MqttMsgExist)
  {
    Serial.println("MQTTRecived");

    if (!strcmp(function, "location"))
    {
      TriggerLocation = clear_all_and_set_trigger();
    }
    else if (!strcmp(function, "flash"))
    {
      TriggerFlash = clear_all_and_set_trigger();
    }
    else if (!strcmp(function, "demo"))
    {
      TriggerDemo = clear_all_and_set_trigger();
    }
    else if (!strcmp(function, "random"))
    {
      TriggerRandom = clear_all_and_set_trigger();
    }
    else if (!strcmp(function, "clear"))
    {
      TriggerClear = clear_all_and_set_trigger();
    }
    MqttMsgExist = false;
  }

  if (TriggerLocation)
  {
    for (int i = 0; i < LED_HEIGHT; i++)
    {
      setLocation(matrix, shelf, i, column, size, 0, 100, 0);
      matrix.drawPixel(x_start, i, matrix.Color(0, 0, 0));
      matrix.drawPixel(x_end - 1, i, matrix.Color(0, 0, 0));
    }

    for (int i = 0; i < LED_WIDTH; i++)
    {
      matrix.drawPixel(i, row, matrix.Color(0, 100, 0));
      
    }
    setLocation(matrix, shelf, row, column, size, FrontColor_r, FrontColor_g, FrontColor_b);
    TriggerLocation = false;
    ledChange = true;
  }

  if (TriggerDemo)
  {
    int color_r[] = {255, 255, 255, 0, 0, 0, 75};
    int color_g[] = {0, 107, 255, 255, 255, 0, 0};
    int color_b[] = {0, 0, 0, 0, 255, 255, 130};
    for (int j = 0; j < LED_HEIGHT; j++)
    {
      for (int i = 0; i < LED_WIDTH; i++)
      {
        matrix.drawPixel(i, j, matrix.Color(color_r[j], color_g[j], color_b[j]));
      }
    }
    ledChange = true;
    matrix.setBrightness(30);}

    if (TriggerFlash)
    {
      for (int i = 0; i < 5; i++)
      {
        setLocation(matrix, shelf, i, column, size, 0, 0, 0);
        matrix.show();

        delay(50);
        setLocation(matrix, shelf, i, column, size, 0, 255, 0);

        matrix.show();

        delay(50);
        TriggerFlash = false;
        TriggerLocation = true;
      }
    }
    if (ledChange){
    matrix.show();
    }
    if (millis() - lastMsgTime > 60000){
      TriggerDemo = true;
      }


  }
  void setup_wifi()
  {

    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }

    randomSeed(micros());

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }

  void callback(char *topic, byte *payload, unsigned int length)
  {
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (unsigned int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
    }
    Serial.println();

    // Deserialize das JSON-Objekt
    DeserializationError error = deserializeJson(data, payload);

    // Überprüfe, ob das Parsing erfolgreich war
    if (error)
    {
      Serial.print("Deserialization failed: ");
      Serial.println(error.c_str());
      return;
    }

    // Greife auf die Werte im JSON-Objekt zu
    function = data["function"];
    row = data["row"];
    shelf = data["shelf"];
    column = data["column"];
    size = data["size"];

    // Gib die Werte aus
    Serial.println("JSON-Daten:");
    Serial.print("Function: ");
    Serial.println(function);
    Serial.print("Row: ");
    Serial.println(row);
    Serial.print("Shelf: ");
    Serial.println(shelf);
    Serial.print("Column: ");
    Serial.println(column);
    Serial.print("Size: ");
    Serial.println(size);
    MqttMsgExist = true;
  }

  void reconnect()
  {
    // Loop until we're reconnected
    while (!client.connected())
    {
      Serial.print("Attempting MQTT connection...");
      // Create a random client ID
      String clientId = "ESP8266Client-";
      clientId += String(random(0xffff), HEX);
      // Attempt to connect
      if (client.connect(clientId.c_str()))
      {
        Serial.println("connected");
        // Once connected, publish an announcement...

        // ... and resubscribe
        client.subscribe(mqtt_InTopic);
      }
      else
      {
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
        // Wait 5 seconds before retrying
        delay(5000);
      }
    }
  }

  bool clear_all_and_set_trigger()
  {

    TriggerLocation = false;
    TriggerFlash = false;
    TriggerDemo = false;
    TriggerClear = false;
    TriggerRandom = false;
    TriggerClear = false;
    lastMsgTime = millis();
    matrix.setBrightness(255);
    
    return true;
  }

  void setLocation(Adafruit_NeoMatrix & matrix, int shelf, int row, int column, const char *size, int rot, int gruen, int blau)
  {
    x_start = 0;
    x_end = 0;
    y_row = row;

    if (!strcmp(size, "small"))
    {
      x_start += int(shelf * LED_PER_SHELF_ROW + LED_START_BOX_KLEIN[column]);
      x_end = x_start + LED_PER_SMALL_BOX;
    }
  else if (!strcmp(size, "large"))
  {
      x_start += int(shelf * LED_PER_SHELF_ROW + LED_START_BOX_GROSS[column]);
      x_end = x_start + LED_PER_LARGE_BOX;
  }
  for (int i = x_start; i < x_end; i++)
  {
      matrix.drawPixel(i, y_row, matrix.Color(rot, gruen, blau));
  }
  }