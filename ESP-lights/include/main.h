#pragma once

#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <secrets.h>

#define PIN D4

#define NUMPIXELS 12 // Popular NeoPixel ring size



// LED
int FrontColor_r = 255;
int FrontColor_g = 255;
int FrontColor_b = 255;

int BackColor_r = 0;
int BackColor_g = 0;
int BackColor_b = 0;

bool ledChange = false;

int LED_WIDTH = 145;
int LED_HEIGHT = 7;
int LED_COUNT = LED_WIDTH * LED_HEIGHT; // # Number of LED pixels.
int LED_PER_SHELF_ROW = 24;
int LED_START_BOX_KLEIN[] = {1, 6, 11, 16, 21};
int LED_START_BOX_GROSS[] = {2, 2, 10, 18, 18};
int LED_PER_SMALL_BOX = 4;
int LED_PER_LARGE_BOX = 5;

int x_start = 0;
int x_end = 0;
int y_row = 0;

int DISPLAY_WIDTH = LED_WIDTH;
int SHELFS_PER_LED_ROW = 6; //  # 0-5 = 6

int SHELF_ROWS = 7;
int SHELF_COLUMNS = 5;

String AVAILABLE_SIZES[] = {"small", "large"};
String AVAILABLE_FUNCTIONS[] = {"location", "flash", "demo", "random", "clear"};

// mqtt
WiFiClient espClient;
PubSubClient client(espClient);

unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(
    LED_WIDTH,
    LED_HEIGHT,
    PIN,
    NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG,
    NEO_GRB + NEO_KHZ800);

JsonDocument data;
const char *function;
int row;
int shelf;
int column;
const char *size;
bool MqttMsgExist = false;
bool TriggerLocation = false;
bool TriggerFlash = false;
bool TriggerDemo = true;
bool TriggerClear = false;
bool TriggerRandom = false;
unsigned long lastMsgTime = 0;

// functions
void setup_wifi();
void callback(char *topic, byte *payload, unsigned int length);
void reconnect();
void setLocation(Adafruit_NeoMatrix &matrix, int shelf, int row, int column, const char *size, int rot, int gruen, int blau);
bool clear_all_and_set_trigger();
