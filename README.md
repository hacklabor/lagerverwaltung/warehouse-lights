# Warehouse Lights

Anzeige der Box-Location über ws2812b und MQTT

## Setup
0. Warehouse-Pi Deploy Key im Repo aktivieren und auf dem Raspberry Pi in ~/.ssh hinterlegen
1. Repo klonen  
    ```bash
    git clone git@gitlab.com:hacklabor/lagerverwaltung/warehouse-lights.git
    ```
2. `secrets.py` anlegen und `MQTT_SERVER = '<Broker-IP>'` eintragen
    ```bash
    cd warehouse-lights
    vi warehouse-lights/secrets.py
    ```
3. Installation der Requirements
    ```bash
    sudo apt-get install libjpeg8-dev
    python3 -m pip install -r requirements.txt    
    ```
4. Start
   ```bash
   sudo python3 -m warehouse-lights.lights 
   ```
## Development
- SSH:
    ```bash
    ssh lager01.hacklabor.de -l pi
    ```
- schnelles Ersetzen des Skripts zum Testen, ohne dass das Einchecken ins Repo notwendig ist:
    ```bash
    scp warehouse-lights/lights.py pi@lager01.hacklabor.de:/home/pi/warehouse-lights/warehouse-lights/lights.py
    ```

## Usage

Das Programm stellt eine Verbindung zu dem in `secrets.py` hinterlegten MQTT-Broker her und subscribed das Topic `WarehouseLights`.

Die Payload der empfangenen MQTT-Messages muss JSON sein. Dieses JSON-Objekt muss einen 'function' key enthalten, der die folgenden Werte annehmen kann: 'location', 'flash', 'demo', 'random', 'clear'.

Wird die 'location' oder 'flash' Funktion zum Anzeigen der Lagerorte aufgerufen, so werden weiterhin die folgenden Parameter benötigt:

| **Parameter** | **Format**                       | **Beispiel**        |
| ------------- | -------------------------------- | ------------------- |
| *row*         | Integer                          | 0                   |
| *shelf*       | Integer                          | 2                   |
| *column*      | Integer                          | 4                   |
| *size*        | String: `small` or `large`       | 'small'            |


Ein einfacher Test mit einem MQTT-Client könnte folgendermaßen aussehen: 
```bash
mosquitto_pub -m "{\"function\":\"demo\"}" -h <Broker-IP> -t WarehouseLights
    -oder-
mosquitto_pub -m "{\"function\":\"location\", \"row\": 0, \"shelf\": 0, \"column\": 0, \"size\": \"small\"}" -h <Broker-IP> -t WarehouseLights
```
