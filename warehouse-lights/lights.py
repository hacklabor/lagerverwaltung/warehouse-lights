#!/usr/bin/env python
import argparse
import json
import os
import signal
import sys
import time
import traceback

import paho.mqtt.client as mqtt
from PIL import Image

from neopixel import *
from secrets import MQTT_SERVER  # add secrets.py file to project dir with content: MQTT_SERVER = '<example>'

# LED strip configuration:
LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 100  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_ZigZac = True  # Leds gehen hin und zurück
LED_WIDTH = 145
LED_HEIGHT = 7
LED_PANEL = 1
LED_COUNT = LED_WIDTH * LED_HEIGHT * LED_PANEL  # Number of LED pixels.
x_start1 = 0
x_end1 = 0
y1 = 0
data = ''
MqttMsgExist = False
DISPLAY_WIDTH = LED_WIDTH * LED_PANEL

SHELFS_PER_LED_ROW = 6  # 0-5 = 6

SHELF_ROWS = 7
SHELF_COLUMNS = 5
LED_PER_SHELF_ROW = 24
LED_START_BOX_KLEIN = (1, 6, 11, 16, 21)
LED_START_BOX_GROSS = (2, 2, 10, 18, 18)
LED_PER_SMALL_BOX = 4
LED_PER_LARGE_BOX = 5

AVAILABLE_SIZES = ['small', 'large']
AVAILABLE_FUNCTIONS = ['location', 'flash', 'demo', 'random', 'clear']

# INIT
randomTime = 0
lastTime = 0

FrontColor_r = 255
FrontColor_g = 255
FrontColor_b = 255

BackColor_r = 0
BackColor_g = 0
BackColor_b = 0

TriggerLocation = False
TriggerFlash = False
TriggerDemo = False
TriggerClear = False
TriggerRandom = True
TriggerClear = False


def signal_handler(sig, frame):
    print("Program exited by user.")
    sys.exit(0)


def on_connect(client, userdata, flags, rc):
    print("Connected with result code {}".format(str(rc)))
    print('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')


def on_disconnect(client, userdata, flags, rc):
    if rc != 0:
        print("Unexpected disconnection.")
        sys.exit(0)


def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    global MqttMsgExist
    global data
    try:
        data = json.loads(msg.payload.decode('utf-8'))

        # Validate input
        assert data['function'] in AVAILABLE_FUNCTIONS, "'function' must be on of the following: {}".format(
            AVAILABLE_FUNCTIONS)
        if data['function'] == 'location' or data['function'] == 'flash':
            assert 0 <= data['row'] < SHELF_ROWS, "'row' must be an integer >=0 and <{}".format(SHELF_ROWS)
            assert 0 <= data['column'] < SHELF_COLUMNS, "'column' must be an integer >=0 and <{}".format(SHELF_COLUMNS)
            assert 0 <= data['shelf'] < SHELFS_PER_LED_ROW, "'shelf' must be an integer between >=0 and <{}".format(
                SHELFS_PER_LED_ROW)
            assert data['size'] in AVAILABLE_SIZES, "'size' must be on of the following: {}".format(AVAILABLE_SIZES)
        MqttMsgExist = True  # Create image
        print("Fertig", MqttMsgExist)
    except json.JSONDecodeError as e:
        print("Error decoding MQTT message: {}\n{}".format(e, msg.payload))

    except KeyError as e:
        print("MQTT message did not contain required key: {}\n{}".format(e, msg.payload))

    except AssertionError as e:
        print("MQTT message incorrect: {}\n{}".format(e, msg.payload))

    except Exception:
        print(traceback.format_exc())


def on_log(client, mqttc, obj, level, string):
    print(string)


def fill_stripe_ram(panelAnzahl, width, height, BMP):
    # print('fillStripeRam')

    for PanelNr in range(panelAnzahl):
        for row in range(height):
            for column in range(width):
                # pixelNr Nummer im Stripe berechnen
                if LED_ZigZac and ((row + 1) & 1):
                    # Gerade Reihen bei Hin und Zurück LED Reihen im Panel
                    pixelNr = (PanelNr * width * height) + column + (row * width)
                else:
                    # ungerade Reihen bei Hin und Zurück LED Reihen im Panel
                    pixelNr = (PanelNr * width * height) + (row * width) + (width - column) - 1
                if not LED_ZigZac:
                    # wenn die letze LED der vorherigen Zeile mit der ersten der Zeile verbunden ist
                    pixelNr = (PanelNr * width * height) + column + (row * width)

                # RGB werte aus dem Bitmap an Spalte, Zeile holen
                r, g, b = BMP.getpixel(((column + PanelNr * width), row))
                # auf die Led mit der vorher berechneten Pixel Nummer schreiben
                strip.setPixelColor(pixelNr, Color(g, r, b))


def clear_all_and_set_trigger():
    global TriggerLocation
    global TriggerFlash
    global TriggerDemo
    global TriggerClear
    global TriggerRandom
    global TriggerClear
    TriggerLocation = False
    TriggerFlash = False
    TriggerDemo = False
    TriggerClear = False
    TriggerRandom = False
    TriggerClear = False
    return True


def setLocation(pixels, shelf, row, column, size, rot, gruen, blau):
    x_start = 0  # erste LED nicht Sichtbar
    x_end = 0
    y_row = row

    if size == 'small':
        x_start += int(shelf * LED_PER_SHELF_ROW + LED_START_BOX_KLEIN[column])
        x_end = x_start + LED_PER_SMALL_BOX
    elif size == 'large':
        x_start += int(shelf * LED_PER_SHELF_ROW + LED_START_BOX_GROSS[column])
        x_end = x_start + LED_PER_LARGE_BOX

    for i in range(x_start, x_end):
        pixels[i, y_row] = (rot, gruen, blau)
    return pixels, x_start, x_end, y_row


if __name__ == "__main__":
    print("Start")
    # Handle Ctrl-C
    signal.signal(signal.SIGINT, signal_handler)

    # Script Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    hostname = MQTT_SERVER  # add secrets.py file to project dir with content: MQTT_SERVER = '<example>'
    response = 1

    # Test connection to MQTT-Broker
    while response != 0:
        print("Trying to reach MQTT-Broker at {}...".format(hostname))
        response = os.system("ping -c 1 " + hostname + " > /dev/null")

        # and then check the response...
        if response != 0:
            print(hostname, 'is Down! Retrying in 5 seconds...')
            time.sleep(5)
    print('MQTT-Broker at {} is available.'.format(hostname))

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    strip.begin()

    # MQTT-Client setup
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message

    client.connect(hostname, 1883, 60)
    client.subscribe("WarehouseLights", 0)
    client.loop_start()

    while True:
        ledChange = False
        aktBMP = Image.new('RGB', (LED_WIDTH * LED_PANEL, LED_HEIGHT),
                           (int(BackColor_r), int(BackColor_g), int(BackColor_b)))
        pixels = aktBMP.load()
        # ----------------------------------------------------------------------------
        # Mqtt message  and variable settings
        # ----------------------------------------------------------------------------
        if MqttMsgExist:
            print("MQTTRecived")
            # Handle Functions
            if data['function'] == 'location':
                TriggerLocation = clear_all_and_set_trigger()

            elif data['function'] == 'flash':
                TriggerFlash = clear_all_and_set_trigger()
            elif data['function'] == 'demo':
                TriggerDemo = clear_all_and_set_trigger()
            elif data['function'] == 'random':
                TriggerRandom = clear_all_and_set_trigger()
            elif data['function'] == 'clear':
                TriggerClear = clear_all_and_set_trigger()
            MqttMsgExist = False

        # ----------------------------------------------------------------------------
        # Function settings
        # ----------------------------------------------------------------------------
        if TriggerLocation:
            for i in range(0, LED_HEIGHT):
                (pixels, x_start1, x_end1, y1) = setLocation(pixels, data['shelf'], i, data['column'],
                                                             data['size'],
                                                             0, 50, 0)
                pixels[x_start1, i] = (0, 0, 0)
                pixels[x_end1 - 1, i] = (0, 0, 0)

            for i in range(0, LED_WIDTH):
                pixels[i, data['row']] = (0, 50, 0)
            (pixels, x_start1, X_end1, y1) = setLocation(pixels, data['shelf'], data['row'], data['column'],
                                                         data['size'],
                                                         FrontColor_r, FrontColor_g, FrontColor_b)
            TriggerLocation = False
            ledChange = True

        if TriggerDemo:
            color_r = [255, 255, 255, 0, 0, 0, 75]
            color_g = [0, 107, 255, 255, 255, 0, 0]
            color_b = [0, 0, 0, 0, 255, 255, 130]
            for j in range(LED_HEIGHT):
                for i in range(LED_WIDTH):
                    pixels[i, j] = (color_r[j], color_g[j], color_b[j])

        if TriggerFlash:
            for i in range(0, 5):
                (pixels, x_start, X_end, y) = setLocation(pixels, data['shelf'], data['row'], data['column'],
                                                          data['size'],
                                                          0, 0, 0)
                fill_stripe_ram(LED_PANEL, LED_WIDTH, LED_HEIGHT, aktBMP)
                strip.show()
                time.sleep(0.05)
                (pixels, x_start, X_end, y) = setLocation(pixels, data['shelf'], data['row'], data['column'],
                                                          data['size'],
                                                          0, 255, 0)
                fill_stripe_ram(LED_PANEL, LED_WIDTH, LED_HEIGHT, aktBMP)
                strip.show()
                time.sleep(0.05)
                TriggerFlash = False
                TriggerLocation = True

        # ---------------------------------------------------------------
        # copy Bitmap to RGB Matrix ram
        if ledChange:
            fill_stripe_ram(LED_PANEL, LED_WIDTH, LED_HEIGHT, aktBMP)
            strip.show()
        # ---------------------------------------------------------------
        # wait for minimum 50ns
        time.sleep(0.1)

        # ---------------------------------------------------------------
        # Write RGB Matrix Hardware

        # print(lastTime)
